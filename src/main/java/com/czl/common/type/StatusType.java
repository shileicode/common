package com.czl.common.type;

public enum StatusType {
    Demand(0,"申请")
    , Test(1,"测试")
    , Valid(2,"正常")
    , Suspend(3,"暂停")
    , Invalid(4,"无效")
    , Lock(5,"锁定")
    , HIDDEN(6,"隐藏")
    , Finish(7,"完成")
    , Cancel(8,"取消")
    , Temp(9,"临时")
    , CallBack(10,"回调")
    , CheckSucc(11,"审核通过")
    , CheckFail(12,"审核失败")
    , Forbidden(13,"被屏蔽")
    , Delete(14,"被删除")
    ;

    private final String info;

    private final int value;

    ////////////////////////////////////////////////////////////////////////////
    //
    // 构造器
    //
    ////////////////////////////////////////////////////////////////////////////

    private StatusType(int value, String info){
        this.info = info;
        this.value = value;
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // 方法
    //
    ////////////////////////////////////////////////////////////////////////////
    public static StatusType valueof(Integer value){
        if(value != null){
            StatusType[] valueArr = StatusType.values();
            for(StatusType status : valueArr){
                if(status.getValue() == value){
                    return status;
                }
            }
        }
        return null;
    }
    ////////////////////////////////////////////////////////////////////////////
    //
    // get set
    //
    ////////////////////////////////////////////////////////////////////////////

    public String getInfo(){
        return info;
    }

    public int getValue(){
        return value;
    }
}

