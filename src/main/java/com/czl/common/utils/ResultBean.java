package com.czl.common.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *@author  hdj
 *@date
 *@description 描述 返回结果实体
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResultBean {
    /**
     * 状态码
     */
    private Integer code;
    /**
     * 信息
     */
    private String message;
    /**
     * 数据
     */
    private Object data;

    public ResultBean(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    public ResultBean(Constant.StatusCode statusCode) {
        this.code = statusCode.value();
        this.message = statusCode.getReasonPhrase();
    }
    public ResultBean(Constant.StatusCode statusCode,Object data) {
        this.code = statusCode.value();
        this.message = statusCode.getReasonPhrase();
        this.data=data;
    }
    public ResultBean(Constant.StatusCode statusCode,String message,Object data) {
        this.code = statusCode.value();
        this.message = message;
        this.data=data;
    }
    public static ResultBean createResultBean(Constant.StatusCode statusCode,Object data) {
       return new ResultBean(statusCode,data );
    }
    public static ResultBean createResultBean(Constant.StatusCode statusCode,String message,Object data) {
       return new ResultBean(statusCode,message,data);
    }
    public static ResultBean createResultBean(Constant.StatusCode statusCode) {
       return new ResultBean(statusCode);
    }
    public static ResultBean createResultBean(Integer code, String message) {
       return new ResultBean(code, message);
    }
}
