package com.czl.common.utils;

public class Constant {
    /**
    *文件上传路径
    * */
    public static final String UPLOAD_PATH="/var/file/static";

    public enum FilePath {
        AAC(10001,".acc","/audio/acc","AAC音频"),

        MP3(10002,".mp3","/audio/mp3","MP3音频"),

        WAV(10003,".wav","/audio/wav","波形音频格式"),

        AVI(20004,".avi","/video/avi","音频视频交错格式"),

        MP4(20005,".mp4","/video/mp4","MP4视频"),

        GIF(30001,".gif","/image/gif","图形交换格式（GIF）"),

        JPEG(30002,".jpeg","/image/jpeg","JPEG图像"),

        JPG(30003,".jpg","/image/jpg","JPG图像"),

        PNG(30004,".png","/image/png","便携式网络图形"),

        JS(40001,".js","/application/js","JavaScript"),

        JAR(40002,".jar","/application/jar","Java存档"),

        JAVA(40003,".java","/application/java","Java文件"),

        PHP(40004,".php","/application/php","php"),

        HTML(40005,".html","/application/html","超文本标记语言（HTML）"),

        XHTML(40006,".xhtml","/application/xml","XHTML"),

        XML(40007,".xml","/application/xml","XML"),

        SQL(40008,".sql","/application/sql","sql"),

        JSON(40009,".json","/application/json","JSON格式"),

        JSONLD(40010,".jsonld","/application/jsonld","JSON-LD格式"),

        BIN(40011,".bin","/application/bin","任何类型的二进制数据"),

        CSH(40012,".csh","/application/csh","C-Shell脚本"),

        CSS(40013,".css","/application/css","级联样式表（CSS）"),

        PDF(40014,".pdf","/application/pdf","Adobe 可移植文档格式（PDF）"),

        PPT(40015,".ppt","/application/ppt","Microsoft PowerPoint"),

        PPTX(40016,".pptx","/application/pptx","Microsoft PowerPoint（OpenXML）"),

        VSD(40017,".vsd","/application/vnd","微软Visio"),

        XLS(40018,".xls","/application/excel","Excel文档"),

        XLSX(40019,".xlsx","/application/excel","Excel文档"),

        DOC(40020,".doc","/application/word","Word文档"),

        DOCX(40021,".docx","/application/word","Word文档"),

        TAR(40022,".tar","/application/tar","磁带存档（TAR）"),

        GZ(40023,".gz","/application/gz","GZip压缩档案"),

        RAR(40024,".rar","/application/rar","RAR档案"),

        ZIP(40025,".zip","/application/zip","ZIP"),

        MIME_7Z(40026,".7z","/application/7z","7-zip存档"),

        TXT(40027,".txt","/text/plain","文本")
        ;
        private int index;
        private String suffix;
        private String path;
        private String describe;

        FilePath(int index, String suffix, String path, String describe) {
            this.index = index;
            this.suffix = suffix;
            this.path = path;
            this.describe = describe;
        }

        public int getIndex() {
            return index;
        }

        public String getSuffix() {
            return suffix;
        }

        public String getPath() {
            return path;
        }

        public String getDescribe() {
            return describe;
        }

        /**
         * 通过文件后缀获取文件上传路径
        * */
        public static String getUploadPath(String suffix) {
            FilePath[] filePath=values();
            for (FilePath f : filePath) {
                if (f.getSuffix().equals(suffix)){
                    return f.getPath();
                }
            }
            return null;
        }
        /**
         * 通过文件后缀获取文件上传路径
        * */
        public static int getUploadIndex(String suffix) {
            FilePath[] filePath=values();
            for (FilePath f : filePath) {
                if (f.getSuffix().equals(suffix)){
                    return f.getIndex()/100;
                }
            }
            return 400;
        }

    }

    /**
     *@author  hdj
     *@date
     *@description 状态码
     */
    public enum StatusCode {


        OK(20000, "成功"),

        ERROR(20001, "失败"),

        LOGINERROR(20002, "用户名密码错误") ,

        REMOTEERROE(20004, "远程调用失败"),

        TIMEERROE(20005, "token失效");



        private final int value;
        private final String reasonPhrase;

        StatusCode(int value, String reasonPhrase) {
            this.value = value;
            this.reasonPhrase = reasonPhrase;
        }

        public int value(){
            return this.value;
        }

        public  String getReasonPhrase(){
            return this.reasonPhrase;
        }
    }
}
