package com.czl.common.ip;

import cn.hutool.core.bean.BeanUtil;

import java.util.Map;

/**
 * @author 石雷
 * shilei1@jiayuan.com
 * 2020/9/22/022 18:21
 */
public class IpInfo {
    private String country;//国家
    private String area;//区域
    private String province;//省份
    private String city;//城市
    private String operator;//运营商

    public IpInfo(String str) {
        if(str!=null){
            String[] strs = str.split("\\|");
            this.country = strs[0];
            this.area = strs[1];
            this.province = strs[2];
            this.city = strs[3];
            this.operator = strs[4];
        }else {
            this.country = "";
            this.area = "";
            this.province = "";
            this.city = "";
            this.operator = "";
        }
    }

    public IpInfo() {
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public String toString() {
        return "IpInfo{" +
                "country='" + country + '\'' +
                ", area='" + area + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", operator='" + operator + '\'' +
                '}';
    }

    public Map<String, Object> toMap(){
        return BeanUtil.beanToMap(this);
    }
}
