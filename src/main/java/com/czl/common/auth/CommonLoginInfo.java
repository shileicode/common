package com.czl.common.auth;



import com.czl.common.auth.bean.AppInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class CommonLoginInfo implements Serializable {


	private static final long serialVersionUID = 8635980985134135533L;
	// 用户id
	protected  Long uid;
	// 客户端平台名称
	protected  String appName;

	protected  String appTag;
	// 登录类型
	protected  String logtype;
	// 登录时间
	protected  Date loginTime;

	protected  AppInfo appInfo;
	// 额外登录参数
	protected Map<String, Object> extraParams;
	// 外部扩展参数
	protected String extendParams;

	public CommonLoginInfo(App app, Long uid, String logtype, Date loginTime, AppInfo appInfo) {
		this.appName=app.getName();
		this.appTag=app.getTag();
		this.appInfo=appInfo;
		this.uid=uid;
		this.logtype=logtype;
		this.loginTime=loginTime;
	}

	public AppInfo getAppInfo() {
		return this.appInfo;
	}

	public void setAppInfo(final AppInfo appInfo) {
		this.appInfo = appInfo;
	}

	public void setUid(final Long uid) {
		this.uid = uid;
	}

	public void setAppName(final String appName) {
		this.appName = appName;
	}

	public void setAppTag(final String appTag) {
		this.appTag = appTag;
	}

	public void setLogtype(final String logtype) {
		this.logtype = logtype;
	}

	public void setLoginTime(final Date loginTime) {
		this.loginTime = loginTime;
	}

	public CommonLoginInfo() {
	}

	//	private HaiwanLoginInfo(HyLoginInfo hyLoginInfo) {
//		this.uid = hyLoginInfo.getUid();
//		this.originalUid = hyLoginInfo.getOriginalUid();
//		this.uidPrefix = hyLoginInfo.getUidPrefix();
//		this.appName = hyLoginInfo.getAppName();
//		this.deviceId = hyLoginInfo.getDeviceId();
//		this.clientId = hyLoginInfo.getClientId();
//		this.channelId = hyLoginInfo.getChannelId();
//		this.versionId = hyLoginInfo.getVersionId();
//		this.sdkVersionNum = ConverterUtils.versionIdToLong(hyLoginInfo.getSdkVersionId(), 3, 0l);
//		this.logtype = hyLoginInfo.getLogtype();
//		this.loginTime = hyLoginInfo.getLoginTime();
//		this.extraParams = hyLoginInfo.getExtraParams();
//		this.extendParams = hyLoginInfo.getExtendParams();
//	}
	

	public Long getUid() {
		return uid;
	}

	public String getAppName() {
		return appName;
	}

	public String getAppTag() {
		return appTag;
	}

	public String getLogtype() {
		return logtype;
	}

	public void setExtraParams(Map<String, Object> extraParams) {
		this.extraParams = extraParams;
	}

	public void setExtendParams(String extendParams) {
		this.extendParams = extendParams;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public Map<String, Object> getExtraParams() {
		return extraParams;
	}

	public String getExtendParams() {
		return extendParams;
	}
}
