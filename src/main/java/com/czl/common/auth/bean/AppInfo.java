package com.czl.common.auth.bean;

import cn.hutool.json.JSONObject;
import com.czl.common.ip.IpUtil;
import com.czl.common.util.HCUtil;
import com.czl.common.util.StringUtils;
import com.czl.common.util.Tools;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


@JsonInclude(Include.NON_NULL)
public class AppInfo implements Serializable {

	private static final long serialVersionUID = -7079163159235501700L;

	private String ip;
	private Map<String,Object> ipArea;
	private String deviceId;//设备唯一标识
		private String clientId;//安卓9902 ios 9901
	private String channelId;//渠道标识
	private String versionId;//app版本
	private String sdkVersionId;//直播sdk版本
	private String mac;//mac地址
	private String model;//设备型号
	private String brand;//厂商
	private String traceid;//设备唯一标识2
	private String oaId;//安卓10以后的 联盟唯一标识
	private String osver;//设备系统版本号
	private String idfa;//ios特有设备码
	private String lang;//语言

	private String hc;//客户端健康码
	private String isJailbreak;//是否root 或越狱
	private String isPiracy;//是否盗版
	private String isSimulator;//是否模拟器
	private String isKernelHijacking;//是否内核劫持
	private String seed;//客户端自己种的种子


	public String getOaId() {
		return oaId;
	}

	public void setOaId(String oaId) {
		this.oaId = oaId;
	}

	public Map<String, Object> getIpArea() {
		return ipArea;
	}

	public void setIpArea(Map<String, Object> ipArea) {
		this.ipArea = ipArea;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getSeed() {
		return seed;
	}

	public void setSeed(String seed) {
		this.seed = seed;
	}

	public String getIsJailbreak() {
		return isJailbreak;
	}

	public void setIsJailbreak(String isJailbreak) {
		this.isJailbreak = isJailbreak;
	}

	public String getIsPiracy() {
		return isPiracy;
	}

	public void setIsPiracy(String isPiracy) {
		this.isPiracy = isPiracy;
	}

	public String getIsSimulator() {
		return isSimulator;
	}

	public void setIsSimulator(String isSimulator) {
		this.isSimulator = isSimulator;
	}

	public String getIsKernelHijacking() {
		return isKernelHijacking;
	}

	public void setIsKernelHijacking(String isKernelHijacking) {
		this.isKernelHijacking = isKernelHijacking;
	}

	public String getIdfa() {
		return idfa;
	}

	public void setIdfa(String idfa) {
		this.idfa = idfa;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getTraceid() {
		return traceid;
	}

	public void setTraceid(String traceid) {
		this.traceid = traceid;
	}

	public String getOsver() {
		return osver;
	}

	public void setOsver(String osver) {
		this.osver = osver;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
//		if (StringUtils.isNotBlank(ip)){
//			IpInfo ipInfo = IpUtil.getIpInfo(ip);
//			this.ipArea=ipInfo.toMap();
//		}
		this.ip = ip;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getSdkVersionId() {
		return sdkVersionId;
	}

	public void setSdkVersionId(String sdkVersionId) {
		this.sdkVersionId = sdkVersionId;
	}

	public void setHc(String hc){
		if(StringUtils.isNotBlank(hc)){
			this.hc=hc;
			Map<String, Object> analysis = HCUtil.analysis(hc);
			if(analysis!=null){
				this.isKernelHijacking= Tools.toString(analysis.get("isKernelHijacking"));
				this.isJailbreak=Tools.toString(analysis.get("isJailbreak"));
				this.isPiracy=Tools.toString(analysis.get("isPiracy"));
				this.isSimulator=Tools.toString(analysis.get("isSimulator"));
				this.seed=Tools.toString(analysis.get("seed"));
			}
		}

	}

	public String getHc() {
		return hc;
	}

	public boolean checkHealth(){
		if("1".equals(this.getIsPiracy())||"1".equals(this.getIsSimulator())||"1".equals(this.getIsKernelHijacking())||"1".equals(this.getIsJailbreak())){
			return false;
		}
	    return true;
	}

	public JSONObject toJsonObject(){
		return new JSONObject(Tools.objectToJsonString(this));
	}

	public String toJsonString(){
	    return Tools.objectToJsonString(this);
	}

	public static AppInfo instance(HttpServletRequest request){
		Map<String, String[]> parameterMap = request.getParameterMap();
		Map<String, String> parMap = new HashMap<>();
		for (Map.Entry<String, String[]> stringEntry : parameterMap.entrySet()) {
			parMap.put(stringEntry.getKey(),stringEntry.getValue()!=null?stringEntry.getValue()[0]:null);
		}
		String ipAddr = IpUtil.getIpAddr(request);
		AppInfo appInfo = new AppInfo();
		appInfo.setOaId(parMap.get("oaid"));
		appInfo.setLang(parMap.get("lang"));
		appInfo.setIdfa(parMap.get("idfa"));
		appInfo.setMac(parMap.get("mac"));

		appInfo.setModel(parMap.get("model"));
		appInfo.setBrand(parMap.get("brand"));
		appInfo.setTraceid(parMap.get("traceid"));
		appInfo.setOsver(parMap.get("osver"));
		appInfo.setIp(ipAddr);
		appInfo.setDeviceId(parMap.get("device_id"));
		appInfo.setClientId(parMap.get("client_id"));
		appInfo.setChannelId(parMap.get("channel_id"));
		appInfo.setVersionId(parMap.get("version_id"));
		appInfo.setHc(parMap.get("hc"));


		return appInfo;
	}

}
