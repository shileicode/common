package com.czl.common.auth;

import cn.hutool.core.convert.Convert;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 石雷
 * shilei1@jiayuan.com
 * 2020/10/12/012 16:26
 */
public enum UserFields {
    ACCOUNT(1,"account","前端id"),
    NICK_NAME(2,"nickName","昵称"),
    AVATAR(3,"avatar","头像"),
    GENDER(4,"gender","性别"),
    BIRTHDAY(5,"birthday","生日"),
    REGION(6,"region","地区 省市县"),
    HEIGHT(7,"height","身高 单位cm"),
    WEIGHT(8,"weight","体重 单位kg"),
    OCCUPATION_CODE(9,"occupationCode","职业"),
    INCOME(10,"income","收入"),
    REMARK(11,"remark","备注/自我介绍"),
    REMARK_OTHER(12,"remark_other","对方备注/期望对方/理想型"),
    OBJECTIVE(13,"objective","目标/企图"),
    PHOTO_ALBUM(14,"photo_album","相册"),
    NOVICE(15,"novice","新人/新用户"),
    VIP(16,"vip","vip"),
    REGISTER_TIME(17,"register_time","注册时间"),
    AGE(18,"age","年龄"),
    CONSTELLATION(19,"constellation","星座"),
    PHONE_NUMBER(20,"phone_number","手机号"),
    COMPLETE_PHONE(21,"complete_phone","带前缀手机号"),
    SUPERIOR(22,"superior","优质标识"),
    INVITE_CODE(23,"invite_code","邀请我的人前端id/我填写的邀请码"),
    REALNAMN_AUTH_INFO(25,"realnamn_auth_info","实名认证信息"),

    LOG_OFF(1001,"log_off","注销前手机号"),
    AVATAR_IS_FACE(1002,"avatar_is_face","头像是否是人脸"),
    ACCOMPANY(2001,"accompany","陪陪标识 0否1是"),
    COLOUR(2002,"colour","黑灰颜色标识"),

    ;

    private Integer fieldId;
    private String fieldName;
    private String desc;

    UserFields(Integer fieldId, String fieldName, String desc) {
        this.fieldId = fieldId;
        this.fieldName = fieldName;
        this.desc = desc;
    }

    public static Map<Integer,UserFields> FIELDS= new HashMap<>();

    static{
        for (UserFields value : UserFields.values()) {
            if(FIELDS.containsKey(value.getFieldId())){
                System.out.println("!!!!!!!!fileId重复!!!!!!: " +value.getFieldId()+"-"+FIELDS.get(value.getFieldId()).getFieldName()+"-"+value.getFieldName());
            }
            FIELDS.put(value.getFieldId(),value);
        }

    }

    public Integer getFieldId() {
        return fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getDesc() {
        return desc;
    }

    public static Set<UserFields> getFieldsByIds(Collection<Integer> fieldIds){
        Set<UserFields> fields = new HashSet<>();
        for (Integer fieldId : fieldIds) {
            UserFields field = FIELDS.get(fieldId);
            if(field!=null){
                fields.add(field);
            }
        }
        return fields;
    }
    public static Set<UserFields> getFieldsByIds(String fieldIds){
        String[] fieldss = fieldIds.split(",");
        Set<Integer> fieldIdss = Arrays.stream(fieldss).map(e -> Convert.toInt(e)).collect(Collectors.toSet());
        return getFieldsByIds(fieldIdss);
    }

    public static String jointFieldIds(UserFields... fields){
        if(fields.length == 1){
            return Convert.toStr(fields[0].getFieldId());
        }else{
            String fieldIds = "";
            for(UserFields field : fields){
                fieldIds += (field.getFieldId()+",");
            }
            return fieldIds;
        }
    }
}
