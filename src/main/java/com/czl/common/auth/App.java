package com.czl.common.auth;

import cn.hutool.core.convert.Convert;
import com.czl.common.util.StringUtils;
import com.czl.common.util.secret.Md5String;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public enum App {

    NONE(0, "", "3EdC_rFV5t6g.NONE", "占位"),
    HL(1, "hl", "3EdC_rFV5t6g.HL", "嗨聊"),
    ;
    private int code;
    private String tag;
    private String secret;
    private String name;
    public static Map<Integer, App> FIELDS = new HashMap<>();
    public static Map<String, App> TagFIELDS = new HashMap<>();

    static {
        for (App value : App.values()) {
            FIELDS.put(value.getCode(), value);
            TagFIELDS.put(value.getTag(), value);
        }
    }


    App(int code, String tag, String secret, String name) {
        this.code = code;
        this.tag = tag;
        this.secret = secret;
        this.name = name;
    }
    public static long delPrefix(String uid){
        if(StringUtils.isNotBlank(uid)){
            App[] values = App.values();
            for(App value : values){
                uid = uid.replace(value.getTag(), "");
            }
            return Convert.toLong(uid, 0l);
        }else{
            return 0l;
        }
    }
    public String getSecret() {
        return secret;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getTag() {
        return tag;
    }

    public String getSign(TreeMap<String,Object> parameters){
        parameters.remove("sign");
        StringBuffer args  = new StringBuffer(this.secret);
        Set<String> keySet = parameters.keySet();
        for (String key : keySet) {
            args.append(key).append(parameters.get(key));
        }
        args.append(this.secret);
        return Md5String.md5(args.toString()).toLowerCase();
    }
}