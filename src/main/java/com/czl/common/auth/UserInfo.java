package com.czl.common.auth;

import cn.hutool.core.convert.Convert;
import com.czl.common.util.Tools;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 石雷
 * shilei1@jiayuan.com
 * 2020/10/14/014 13:59
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfo {

    private Long originalUid;

    private String uid;

    /**
     * 前端id
     */
    private Long account;

    /**
     * 登录手机号（不带国家码）
     */
    private String phoneNumber;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 昵称
     */
    private String nickName;


    /**
     * 出生日期
     */
    private String birthday;

    /**
     * 地区
     */
    private Map<String,Object> region;
    /**
     * 身高(cm)
     */
    private BigDecimal height;

    /**
     * 体重(kg)
     */
    private BigDecimal weight;

    /**
     * 职业code码
     */
    private Map<String,Object> occupation;

    /**
     * 收入
     */
    private String income;


    /**
     * 备注 自我介绍
     */
    private String remark;

    /**
     * 对方备注/理想型
     */
    private String remarkOther;

    /**
     * 目标/企图
     */
    private String objective;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 相册
     */
    private List<Map<String,Object>> photoAlbum;

    /**
     * 是否是新用户
     */
    private Integer novice;

    /**
     * vip
     */
    private Map<String,Object> vip;

    /**
     * 注册时间
     */
    private Date registerTime;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 星座
     */
    private String constellation;

    /**
     * 带前缀手机号
     */
    private String completePhone;

    /**
     * 优质标识
     */
    private Integer superior;
    /**
     * 实名信息
     */
    private Map<String,Object> realnamnAuthInfo;

    private String inviteCode;

    private Integer accompany;

    private Integer colour;

    private Integer userStatus;



    //////////////////////////////////////////////////////////////////
    //
    // 自定义方法
    //
    //////////////////////////////////////////////////////////////////

    public String appUid(App app){
        String uid = Convert.toStr(this.getOriginalUid());
        if(app != null){
            uid = app.getTag()+uid;
        }
        return uid;
    }


    //过滤敏感字段
    public UserInfo checkSensitive(){
        if (StringUtils.isNotBlank(this.completePhone)){
            this.completePhone=this.completePhone.substring(0, 6) + "****" + completePhone.substring(10);
        }
        if (StringUtils.isNotBlank(this.phoneNumber)){
            this.phoneNumber=this.phoneNumber.substring(0, 3) + "****" + phoneNumber.subSequence(7, phoneNumber.length());
        }
        if (this.realnamnAuthInfo!=null&& Tools.toLong(this.realnamnAuthInfo.get("has"))==1){
            String realName = Tools.toString(this.realnamnAuthInfo.get("realName"), "");
            String cardNumber = Tools.toString(this.realnamnAuthInfo.get("cardNumber"), "");
            String realName2="";
            if (realName.length()>1){
                realName2=realName.substring(0,1);
                for (int i = 0; i < realName.length()-1; i++) {
                    realName2+="*";
                }
                this.realnamnAuthInfo.put("realName",realName2);
            }
            if (cardNumber.length()>1){
                cardNumber=cardNumber.substring(0, 3) + "**************" + cardNumber.substring(17);
                this.realnamnAuthInfo.put("cardNumber",cardNumber);
            }
        }
        return this;
    }





}
