package com.czl.common.auth;


import java.util.HashMap;
import java.util.Map;

/**
 * @author 石雷
 * shilei1@jiayuan.com
 * 2020/9/22/022 14:54
 */
public enum TokenType {

    LONG_TOKEN(1,60*60*24*30L,"长token"),//长token一个月
    SHORT_TOKEN(2,60*30L,"短token");//短token半个小时

    private int code;
    private long expireTime;
    private String info;
    public static Map<Integer,TokenType> FIELDS= new HashMap<>();
    static {
        for (TokenType value : TokenType.values()) {
            FIELDS.put(value.getCode(),value);
        }
    }

    private static  final  String shortTokenKey="common_short_token_key_"+"common_short_token_key_".hashCode();
    private static  final  String longTokenKey="common_long_token_key_"+"common_long_token_key_".hashCode();


    TokenType(Integer code,Long expireTime,String info){
        this.code=code;
        this.info=info;
        this.expireTime=expireTime;
    }

    public int getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public String getTokenKey(){
        if(this.code==1){
            return shortTokenKey;
        }else {
            return longTokenKey;
        }
    }

}
