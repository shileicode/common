package com.czl.common.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

	public final static long MILLISECOND_OF_DAY = 24l * 60 * 60 * 1000;
	public final static long SECOND_OF_DAY = 24l * 60 * 60;

	public static final int LENGTH_OF_DAY = 86400;
	public static final int LENGTH_OF_HOUR = 3600;
	public static final int LENGTH_OF_MINUTE = 60;
	public static final SimpleDateFormat STF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

	public static String getTimeString(int day, String format){
		return getTimeString(System.currentTimeMillis() + day * MILLISECOND_OF_DAY, format);
	}

	public static String getTimeString(long time, String format){

		if(format == null) {
			return null;
		}

		if(time < 0) {
			time = System.currentTimeMillis();
		}

		return new SimpleDateFormat(format).format(new Date(time));
	}

	public static Long getTimeMillis(String timeString, String format){
		Date date = getDateTime(timeString, format);
		return (date != null) ? date.getTime() : null;
	}

	public static Date getDateTime(String timeString, String format){

		if(timeString == null || timeString.length() != format.length()){
			return null;
		}

		Date date = null;

		try {
			date = new SimpleDateFormat(format).parse(timeString);
		} catch (ParseException e) {
			date = null;
		}

		return date;
	}

	public static Calendar getCalendar(String timeString, String format){

		Date date = getDateTime(timeString, format);
		if(date == null) {
			return null;
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		return calendar;
	}

	public static Date getDate(int dayLength){
		Date now = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(now); // 设置当前日期
		c.add(Calendar.DATE, dayLength);
		return c.getTime();
	}

	public static Date getDate(Date startDate, int dayLength) {
		Calendar c = Calendar.getInstance();
		c.setTime(startDate); // 设置当前日期
		c.add(Calendar.DATE, dayLength);
		return c.getTime();
	}

	public static int getBetweenDays(Date date1, Date date2){
		long between = (date1.getTime() - date2.getTime()) / (1000 * 24 * 60 * 60);
		return (int) (between);
	}

	private static Date addmonth(Date date, int addMonth) {
		Calendar calendar = Calendar.getInstance();
		date.setTime(date.getTime());
		calendar.add(Calendar.MONTH, addMonth);
		return calendar.getTime();
	}

	public static Date addDay(Date date, int days) {
		Calendar calendar = Calendar.getInstance();
		date.setTime(date.getTime());
		calendar.add(Calendar.DATE, days);
		return calendar.getTime();
	}

	/**
	 * 计算d1比d2大多少秒
	 *
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static long getSecond(Date d1, Date d2) {
		long a1 = d1.getTime();
		long a2 = d2.getTime();
		long a3 = (a1 - a2) / 1000;

		return a3;
	}

	public static String formatDate(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}

	public static String formatDate(long dateTime, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(new Date(dateTime));
	}


	public static String formatTime(Date date) {
		return STF.format(date);
	}

	public static String formatDate(Date date) {
		return SDF.format(date);
	}

	/**
	 * 根据所秒数,计算相差的时间并以**时**分**秒返回
	 * @param m
	 * @return
	 */
	public static String getBeapartDate(long m) {
		String beapartdate = "";
		int nDay = (int) m / (LENGTH_OF_DAY);
		// int nHour = (int) (m - nDay * 24 * 60 * 60) / (60 * 60);
		int nHour = (int) (m - nDay * LENGTH_OF_DAY) / (LENGTH_OF_HOUR);
		int nMinute = (int) (m - nDay * LENGTH_OF_DAY - nHour * LENGTH_OF_HOUR) / LENGTH_OF_MINUTE;
		int nSecond = (int) m - nDay * LENGTH_OF_DAY - nHour * LENGTH_OF_HOUR - nMinute * 60;
		beapartdate = nDay + "天" + nHour + "小时" + nMinute + "分" + nSecond + "秒";

		return beapartdate;
	}


	public static long getDayLength(Date startDate, Date endDate){
		long m = endDate.getTime() - startDate.getTime();
		long dayLength = (m / 1000) / LENGTH_OF_DAY;
		return dayLength;
	}


	public static Date parseDate(String date, String pattern) {

		if (null == date)
		{
			return new Date();
		}

		SimpleDateFormat sf = null;
		if (null != pattern)
		{
			sf = new SimpleDateFormat(pattern);
		}
		if (null == sf)
		{
			if (date.contains("GMT"))
			{
				sf = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss 'GMT'Z", Locale.ENGLISH);
			}
			else if (date.contains("UTC"))
			{
				sf = new SimpleDateFormat("EEE MMM dd HH:mm:ss 'UTC'Z yyyy", Locale.ENGLISH);
			}
			else if (date.contains("T"))
			{
				sf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			}
			else if (date.contains(":"))
			{
				sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			}
			else
			{
				sf = new SimpleDateFormat("yyyy-MM-dd");
			}
		}
		try
		{
			return sf.parse(date);
		} catch (ParseException e)
		{
			e.printStackTrace();
		}

		return null;
	}


	public static String getConstellation(int month, int day) {
		int[] dayArr = new int[] { 20, 19, 21, 20, 21, 22, 23, 23, 23, 24, 23, 22 };
		String[] constellationArr = new String[] { "摩羯座", "水瓶座", "双鱼座", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "摩羯座" };
		return day < dayArr[month - 1] ? constellationArr[month - 1] : constellationArr[month];
	}

	public static int getAge(Date birthday)
	{
		Calendar cal_birthday = Calendar.getInstance();
		Calendar cal_now = Calendar.getInstance();
		cal_birthday.setTime(birthday);
		return (cal_now.get(Calendar.YEAR) - cal_birthday.get(Calendar.YEAR));
	}

	/**
	 * 判断是否是当天时间
	 * @param date
	 * @return
	 */
	public static Boolean isToday(Date date){
		Date now = new Date();
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		String nowDay = sf.format(now);
		String day = sf.format(date);
		return day.equals(nowDay);
	}

	/**
	 * @title 获取当前星期的周一
	 * @param date 当前日期
	 * @author 邓毅
	 */
	public static Date getThisWeekMonday(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// 获得当前日期是一个星期的第几天
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
		if (1 == dayWeek) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}
		// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		// 获得当前日期是一个星期的第几天
		int day = cal.get(Calendar.DAY_OF_WEEK);
		// 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
		int firstDayOfWeek = cal.getFirstDayOfWeek();
		cal.add(Calendar.DATE, firstDayOfWeek - day);
		return cal.getTime();
	}

	/**
	 * @title 获取当前星期的周日
	 * @param date 当前日期
	 * @author 邓毅
	 */
	public static Date getThisWeekSunday(Date date) {
		Date monday = getThisWeekMonday(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(monday);
		calendar.add(Calendar.DATE, 6);
		Date time = calendar.getTime();
		return time;
	}

	/**
	 * @title 获取上周一
	 * @param date 当前日期
	 * @author 邓毅
	 */
	public static Date getLastWeekMonday(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(getThisWeekMonday(date));
		cal.add(Calendar.DATE, -7);
		return cal.getTime();
	}

	/**
	 * @title 获取上周日
	 * @param date 当前日期
	 * @author 邓毅
	 */
	public static Date getLastWeekSunday(Date date) {
		Date lastWeekMonday = getLastWeekMonday(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(lastWeekMonday);
		calendar.add(Calendar.DATE, 6);
		Date time = calendar.getTime();
		return time;
	}


	/**
	 * 获取当月第一天00:00:00
	 * @param date
	 * @return
	 */
	public static Date getFirstDayOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH,1);
		calendar.set(Calendar.HOUR_OF_DAY,0);
		calendar.set(Calendar.MINUTE,0);
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MILLISECOND,0);
		Date time = calendar.getTime();
		return time;
	}

	/**
	 *
	 * @param
	 * @return返回周几
	 * @throws Exception
	 */
	// 2，返回数字
	public static int dayForWeek(Date date)  {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dayForWeek = 0;
		try{
			if (c.get(Calendar.DAY_OF_WEEK) == 1) {
				dayForWeek = 7;
			} else {
				dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		return dayForWeek;
	}

	public static int getHour(){
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		return hour;
	}

	public static int getMinutes(){
		Calendar calendar = Calendar.getInstance();
		int minute = calendar.get(Calendar.MINUTE);
		return minute;
	}

	/**
	 * 日期偏移后的Date
	 *
	 * @param date     操作的date
	 * @param add      偏移天数
	 * @param boundary 【1】23:59:59【-1】00:00:00
	 * @return
	 */
	public static Date dateShiftByDay(Date date, int add, int boundary) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, add);
		switch (boundary) {
			case -1:
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				break;
			case 1:
				cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
				cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
				cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
				cal.set(Calendar.MILLISECOND, 0);// MySQL不能存储毫秒, 设为0以确保MySQL存储为当天
				break;
		}
		return cal.getTime();
	}
}
