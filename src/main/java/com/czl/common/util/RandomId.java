package com.czl.common.util;

import cn.hutool.core.convert.Convert;

public class RandomId {

    public static Long createAccountId(long uid){
        long endRandom=getRandom();
        long l= uid*100+ endRandom;
        int pRandom=getRandom();
        pRandom=pRandom<10?pRandom+10:pRandom;
        Long id = Convert.toLong(pRandom + "" + l);
        return id;
    }

    private  static Integer getRandom(){
        Integer integer =null;

        while (integer==null){
            integer=Convert.toInt(Math.random() * 100);

            //小于两位 补位
            integer=integer<10?integer+10:integer;

            //叠号重新获取
            if(integer%11==0) {
//                System.out.println("AA=========="+integer);
                integer=null;
                continue;
            }

            //连号重新获取
            int index1 =integer/10;
            int index2 =integer%10;
            if(Math.abs(index1-index2)==1) {
//                System.out.println("AB=========="+integer);
                integer=null;
            }
        }
        return integer;
    }

}