package com.czl.common.util;

import java.util.*;

@SuppressWarnings("unchecked")
public class Collections {
	
	public static <E> ArrayList<E> newArrayList(){
		return new ArrayList<E>();
	}
	
	public static <E> ArrayList<E> newArrayList(Collection<E> elements){
		return pushElements(new ArrayList<E>(), elements);
	}
	
	public static <E> ArrayList<E> newArrayList(E... elements){
		return pushElements(new ArrayList<E>(), elements);
	}
	
	public static <E> HashSet<E> newHashSet(){
		return new HashSet<E>();
	}
	
	public static <E> HashSet<E> newHashSet(Collection<E> elements){
		return pushElements(new HashSet<E>(), elements);
	}
	
	public static <E> HashSet<E> newHashSet(E... elements){
		return pushElements(new HashSet<E>(), elements);
	}

	public static <E> HashSet<E> newLinkedHashSet(E... elements){
		return pushElements(new LinkedHashSet<E>(), elements);
	}

	public static <E> TreeSet<E> newTreeSet(E... elements){
		return pushElements(new TreeSet<E>(), elements);
	}

	public static <E> TreeSet<E> newTreeSet(Collection<E> elements){
		return pushElements(new TreeSet<E>(), elements);
	}
	
	public static boolean isEmpty(Collection<?> set){
		return (set == null || set.isEmpty());
	}
	
	public static boolean isNotEmpty(Collection<?> set){
		return !isEmpty(set);
	}
	
	private static <T extends Collection<E>, E> T pushElements(T c, E... elements){
		
		if(elements != null && elements.length > 0){
			java.util.Collections.addAll(c, elements);
		}
		
		return c;
	}
	
	private static <T extends Collection<E>, E> T pushElements(T c, Collection<E> elements){
		
		if(isNotEmpty(elements)){
			c.addAll(elements);
		}
		
		return c;
	}
	
}