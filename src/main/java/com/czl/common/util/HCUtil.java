package com.czl.common.util;



import java.util.HashMap;
import java.util.Map;

/**
 * 客户端健康码解析工具
 * @author 石雷
 * shilei1@jiayuan.com
 * 2020/6/1/001 17:18
 */
public class HCUtil {

    public static Map<String,Object> analysis(String hc){

        if(hc.length()<=5||!hc.contains("#")) return null;

        String[] hcs = hc.split("#");
        String hc0=hcs[0];
        int result = Tools.toInt(Integer.toBinaryString(Tools.toInt(hc0)));
        String format = String.format("%08d", result);
        StringBuffer sb = new StringBuffer(format);
        String afterReverse = sb.reverse().toString();
        char s0=afterReverse.charAt(0);
        char s1=afterReverse.charAt(1);
        char s2=afterReverse.charAt(2);
        char s3=afterReverse.charAt(3);
        char s4=afterReverse.charAt(4);
        String hc1=hcs[1];
        Map<String, Object> hcMap = new HashMap<>();
        hcMap.put("deviceType",s0);
        hcMap.put("isJailbreak",s1);
        hcMap.put("isPiracy",s2);
        hcMap.put("isSimulator",s3);
        hcMap.put("isKernelHijacking",s4);
        hcMap.put("seed",hc1);
        return hcMap;
    }


}
