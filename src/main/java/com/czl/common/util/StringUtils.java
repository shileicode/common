package com.czl.common.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	public static final char[] BlankAndReturnChar = new char[]{' ', '\t', '\r', '\n'};

	public final static boolean isBlank(String str) {
		return str == null || str.trim().isEmpty();
	}
	
	public final static boolean isNotBlank(String str) {
		return !isBlank(str);
	}
	
	/**
	 * 判断字符串数组是否全部为空
	 *
	 * @param strs
	 * @return
	 */
	public final static boolean allIsBlank(String... strs) {
		
		if(strs == null || strs.length <= 0) {
			return true;
		}
		
		for(int i = 0, len = strs.length; i < len; i++) {
			if(isNotBlank(strs[i])) {
				return false;
			}
		}

		return true;
	}
	
	/**
	 * 判断字符串数组是否全部不为空
	 * 
	 * @param strs
	 * @return
	 */
	public final static boolean allIsNotBlank(String... strs) {
		
		if(strs == null || strs.length <= 0) {
			return false;
		}
		
		for(int i = 0, len = strs.length; i < len; i++) {
			if(isBlank(strs[i])) {
				return false;
			}
		}

		return true;
	}
	
	public final static boolean checkLength(String str, Integer minLength, Integer maxLength) {
		
		if(str == null) {
			return (minLength == null);
		}
		
		if(minLength != null && str.length() < minLength.intValue()) {
			return false;
		}
		
		if(maxLength != null && str.length() > maxLength.intValue()) {
			return false;
		}
		
		return true;
	}

	public final static boolean checkTrimLength(String str, Integer minLength, Integer maxLength) {
		return checkLength((str != null ? str.trim() : null), minLength, maxLength);
	}
	
	/**
	 * 截取字符串
	 * 
	 * @param str
	 * @param beginIndex
	 * @param endString
	 * 
	 */
	public final static String substring(String str, int beginIndex, String endString, String defaultValue) {
		
		if(str == null || str.length() <= beginIndex) {
			return defaultValue;
		}
		
		int endIndex = str.indexOf(endString, beginIndex);
		if(endIndex < 0) {
			return defaultValue;
		}
		
		return str.substring(beginIndex, endIndex);
	}

	public static final String trimBlankAndReturnChar(String str) {
		return removeAllChars(str, BlankAndReturnChar);
	}

	public static final String removeAllChars(String str, char[] key) {
		if (isNotBlank(str) && key != null && key.length > 0) {
			StringBuffer strbuf = new StringBuffer();
			for(int i = 0; i < str.length(); ++i) {
				char c = str.charAt(i);
				if (!isKeyChar(c, key)) {
					strbuf.append(c);
				}
			}

			return strbuf.toString();
		} else {
			return str;
		}
	}
	public static final boolean isKeyChar(char c, char[] key) {
		if (key != null && key.length > 0) {
			char[] var2 = key;
			int var3 = key.length;

			for(int var4 = 0; var4 < var3; ++var4) {
				char cc = var2[var4];
				if (c == cc) {
					return true;
				}
			}
		}

		return false;
	}

	//移除最后的指定字符
	public static String rtrim(String str,String s) {
		int num=str.length();
		for (int i =  num- 1; i > -1; i--) {
			if (!(str.substring(i,i+1).equals(s))) {
				return str.substring(0, i+1);
			}
		}
		return str;
	}



	private static Pattern linePattern = Pattern.compile("_(\\w)");
	private static Pattern humpPattern = Pattern.compile("[A-Z]");

	/**
	 * 下划线转驼峰
	 * @param str
	 * @return
	 */
	public static String lineToHump(String str) {
		str = str.toLowerCase();
		Matcher matcher = linePattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	/**
	 * 驼峰转下划线
	 * @param str
	 * @return
	 */
	public static String humpToLine(String str) {
		Matcher matcher = humpPattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	/**
	 * 对象转下划线json
	 * @param object
	 * @return
	 * @throws JsonProcessingException
	 */
	public static String toUnderlineJSONString(Object object) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		String reqJson = mapper.writeValueAsString(object);
		return reqJson;
	}

	/**
	 * 下划线json转对象
	 * @param json
	 * @param clazz
	 * @return
	 * @throws IOException
	 */
	public static <T> T toHumpObject(String json, Class<T> clazz) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
		T reqJson =  mapper.readValue(json, clazz);
		return reqJson;
	}
}
