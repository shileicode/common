package com.czl.common.util;

import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机数工具类
 *
 * @author zhangch
 * 创建日期：2019-11-15
 *
 */
public class RandomNumberUtils {

	/**
	 * 获取n位随机数字
	 */
	public static Integer getRandomNumber(int length){

		if(length <= 0){
			throw new IllegalArgumentException("length must be positive");
		}

		int numStr = ThreadLocalRandom.current().nextInt(1, 10);
		for(int i = 1; i < length; i++){
			numStr = numStr * 10 + ThreadLocalRandom.current().nextInt(0, 10);
		}

		return numStr;
	}

	/**
	 * 获取n位不重复的随机数字
	 *
	 * @param length 随机数长度（length不可以超过9）
	 *
	 */
	public static Integer getUnrepeatableRandomNumber(int length){

		if(length <= 0 || length > 9){
			throw new IllegalArgumentException("length must be between 1 and 9");
		}

		int[] numPool = new int[]{1,2,3,4,5,6,7,8,9};

		int numStr = 0;
		for(int i = 0; i < length; i++){
			int index = ThreadLocalRandom.current().nextInt(i, numPool.length);
			numStr = numStr * 10 + numPool[index];
			numPool[index] = numPool[i];
		}

		return numStr;
	}

	public static int getrandom(int start,int end) {

		int num=(int) (Math.random()*(end-start+1)+start);
		return num;
	}
}
