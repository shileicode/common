package com.czl.common.util.secret;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class SecretUtils {

	public static String toMd5(String str) {
		return toMd5(toBytes(str, null));
	}

	public static String toMd5(byte[] bytes) {
		
		if(bytes == null){
			return null;
		}
		
		return Md5String.md5(bytes);
	}

	public static String toSha1(String str) {
		return Md5String.sha1(toBytes(str, null));
	}

	public static String toSha1(byte[] bytes) {
		
		if(bytes == null){
			return null;
		}

		return Md5String.sha1(bytes);
	}

	private static byte[] toBytes(String str, Charset charset){
		
		if(str == null){
			return null;
		}
		
		if(charset == null) {
			charset = StandardCharsets.UTF_8;
		}
		
		return str.getBytes(charset);
	}
	
}
