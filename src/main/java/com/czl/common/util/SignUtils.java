package com.czl.common.util;



import com.czl.common.util.secret.Md5String;


import java.util.Set;
import java.util.TreeMap;

/**
 * Description:签名工具类
 * User: 石雷
 * Date: 2019-09-11
 * Time: 10:29
 */
public class SignUtils {

    /**
     *
     * @param parameters
     * @return
     */
    public static String getSign(TreeMap<String, Object> parameters){
        String secret = "3EdC_rFV5t6gUser";
        parameters.remove("sign");
        StringBuffer args  = new StringBuffer(secret);
        Set<String> keySet = parameters.keySet();
        for (String key : keySet) {
            args.append(key).append(parameters.get(key));
        }
        args.append(secret);
        return Md5String.md5(args.toString()).toLowerCase();
    }
}
