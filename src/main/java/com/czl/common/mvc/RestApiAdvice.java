package com.czl.common.mvc;//package com.azx.common.mvc;
//
//import com.azx.common.log.logger.MyLogger;
//import org.springframework.web.bind.MissingServletRequestParameterException;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
////import org.springframework.web.servlet.NoHandlerFoundException;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * 如果缺少任何的属性或者方法，一定要在这里添加，不要在子类中添加
// *
// * @author zhangch
// * 创建日期：2020-01-15
// *
// */
//@RestControllerAdvice
//public abstract class RestApiAdvice<T> {
//
//	@SuppressWarnings("unused")
//	private MyLogger logger;
//
//	public RestApiAdvice() {}
//
//	public RestApiAdvice(MyLogger logger) {
//		this.logger = logger;
//	}
//
//	@ExceptionHandler(MissingServletRequestParameterException.class)
//	public T missingServletRequestParameterHandler(HttpServletRequest request,
//                                                   HttpServletResponse response,
//                                                   MissingServletRequestParameterException e) {
//
//		int retCode = 400;
//		String errorMsg = "缺少参数";
//
//		T result = createResult(request, response, e, retCode, errorMsg);
//		writeLog(request, response, e, retCode, errorMsg, result);
//
//		return result;
//	}
//
//	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
//	public T methodArgumentTypeMismatchHandler(HttpServletRequest request,
//                                               HttpServletResponse response,
//                                               MethodArgumentTypeMismatchException e) {
//
//		int retCode = 400;
//		String errorMsg = "参数类型错误";
//
//		T result = createResult(request, response, e, retCode, errorMsg);
//		writeLog(request, response, e, retCode, errorMsg, result);
//
//		return result;
//	}
//
//	@ExceptionHandler(NoHandlerFoundException.class)
//	public T noHandlerFoundExceptionhHandler(HttpServletRequest request,
//                                             HttpServletResponse response,
//                                             NoHandlerFoundException e) {
//
//		int retCode = 404;
//		String errorMsg = "接口路径不存在";
//
//		T result = createResult(request, response, e, retCode, errorMsg);
//		writeLog(request, response, e, retCode, errorMsg, result);
//
//		return result;
//	}
//
//	protected abstract T createResult(HttpServletRequest request, HttpServletResponse response, Exception e, Integer retCode, String errorMsg);
//	protected void writeLog(HttpServletRequest request, HttpServletResponse response, Exception e, Integer retCode, String errorMsg, T result) {
//
//	}
//
//}
