package com.czl.common.mvc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Calendar;

/**
 * API接口注解类
 * 在controller方法上使用
 * @author liangcai
 *
 */
@Target(value={ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RestApi {

	String group() default "";
	String name() default "";
	String title() default "";
	boolean login() default false;
	boolean check() default false;
	boolean innet() default false;//是否限制内网访问
	boolean paychek() default false;
	boolean openApi() default false;//是否是开放接口
	String orderSource() default "0";//接口统计标识
	String extOrderSourceParams() default "0";//扩展参数名
	boolean https() default false;//是否要求HTTPS请问
	boolean isWirte() default false;//是否是写接口
	boolean sign() default false;//是否处理验签

	boolean checkRepeat() default false;//是否频繁请求限制
	int checkRepeatMax() default 10;//一个周期人限制次数
	int checkRepeatCycle() default  Calendar.MINUTE;//限制周期 默认秒
	String checkRepeatField() default "";//限制字段

	boolean appInfo() default false;//是否需要appInfo字段信息



	
}
