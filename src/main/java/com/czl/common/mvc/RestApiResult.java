package com.czl.common.mvc;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * api
 */
public class RestApiResult implements Serializable {

    private static final long serialVersionUID = -2078396341144531949L;

    public enum RetCode {

        FAIL("失败", -1),
        SUCCESS("成功", 1),
        UNAUTH("未授权", -401),
        LTokenInvalid("长token失效", -431),
        EXCEPTION("服务器错误", -500),
        SIGNERROR("签名错误", -411),
        JYRCHERROR("rh、ch错误", -412),
        Intercept("拦截层", 111),
        Toast("提示层", 222),
        Alert("提示框", 333),
        Unsafe("请使用HTTPS请求", -420),
        ILLEGAL("非法请求", -430),
        ACCOUNT_EXCEPTION("账号限制", -440),
        DataEmpty("数据结果为空", 1000),
        H5Alert("H5提示框", 1001),
        ;

        private String info;

        private int value;

        private RetCode(String info, int value) {
            this.info = info;
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public String getInfo() {
            return this.info;
        }
    }

    private Integer retCode = 1;// 1为唯一正确标识

    private String msg = "";

    private String orderSource = "";

    private Object data;
    // json 操作后跳转
    private Object jump;
    // 下一步操作。 取值范围0-9，0：不做任何操作，1：toast提示msg值，2：弹层提示msg值，3：待定 ； 默认值0
    private int next = 0;

    public static RestApiResult create(RetCode retCode, String msg) {
        return create(retCode, msg, null, null, 0);
    }

    public static RestApiResult create(Integer retCode, String msg) {
        return create(retCode, msg, null, null, 0);
    }

    public static RestApiResult create(RetCode retCode, String msg, Object data) {
        return create(retCode.value, msg, data, null, 0);
    }

    public static RestApiResult create(Integer retCode, String msg, Object data) {
        return create(retCode, msg, data, null, 0);
    }

    public static RestApiResult create(RetCode retCode, String msg, Object data, Object jump) {
        return create(retCode.value, msg, data, jump, 0);
    }

    public static RestApiResult create(Integer retCode, String msg, Object data, Object jump) {
        return create(retCode, msg, data, jump, 0);
    }

    public static RestApiResult create(RetCode retCode, String msg, Object data, Object jump, int next) {
        return create(retCode.value, msg, data, jump, next);
    }

    public static RestApiResult create(Integer retCode, String msg, Object data, Object jump, int next) {
        RestApiResult apiResult = new RestApiResult();
        apiResult.setData(data);
        apiResult.setRetCode(retCode);
        apiResult.setMsg(msg);
        apiResult.setJump(jump);
        apiResult.setNext(next);
        return apiResult;
    }

    public Integer getRetCode() {
        return retCode;
    }

    public void setRetCode(Integer retCode) {
        this.retCode = retCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> res = new HashMap<String, Object>();
        res.put("retCode", this.getRetCode());
        res.put("msg", this.getMsg());
        res.put("next", this.getNext());
        if (this.getData() != null)
            res.put("data", this.getData());

        if (this.getJump() != null)
            res.put("jump", this.getJump());

        return res;

    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public Object getJump() {
        return jump;
    }

    public void setJump(Object jump) {
        this.jump = jump;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }
}