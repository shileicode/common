package com.czl.common.mvc;

import cn.hutool.core.convert.Convert;
import com.czl.common.ip.IpUtil;
import com.czl.common.util.Tools;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Map;

//import org.springframework.web.servlet.HandlerMapping;

public abstract class BaseRestApiAop {
	
    protected String getOrderSource(HttpServletRequest request, String orderSource, String extOrderSourceParams, String exOrderSourcesParam) {
    	
    	orderSource = Convert.toStr(orderSource, "0");
		if (StringUtils.isNotBlank(extOrderSourceParams)) {
			String[] params = extOrderSourceParams.split(",");
			for(int i = 0, len = (params != null) ? params.length : 0; i < len; i++) {
				String extendOrderSource = request.getParameter(params[i]);
				if(StringUtils.isBlank(extendOrderSource)) {
					continue;
				}
				orderSource = orderSource + "_" + extendOrderSource;
			}
		}

		String extraOrderSourcesStr = Tools.toString(request.getParameter(exOrderSourcesParam), "");
		if (StringUtils.isNotBlank(extraOrderSourcesStr)) {
			// 防止客户端bug导致无线刷
			String[] extraOrderSources = extraOrderSourcesStr.split("\\.");
			if (extraOrderSources != null && extraOrderSources.length < 20) {
				orderSource = extraOrderSourcesStr + "." + orderSource;
			}
		}
		
		return orderSource;
    }
    
    protected String getRequestLogStr(HttpServletRequest request) {
    	
    	Map<String, String[]> params = request.getParameterMap();
    	if(params == null) {
    		return "";
    	}
    	
    	StringBuilder logStr = new StringBuilder("");
    	
    	for(Map.Entry<String, String[]> e : params.entrySet()) {
    		
    		String value = null;
    		if(e.getValue() == null) {
    			value = "";
    		} else if(e.getValue().length == 1) {
    			value = e.getValue()[0];
    		} else {
    			value = Arrays.toString(e.getValue());
    		}
    		
    		logStr.append(Tools.toString(e.getKey(), "")).append("=").append(value).append("&");
    	}
    	
    	if(logStr.length() > 0) {
    		logStr.setLength(logStr.length() - 1);
    	}
    	
    	return logStr.toString();
    }
	
	
	protected String getLogStr(HttpServletRequest request, Integer retCode, Long time, Object result, Exception e) {
		StringBuilder logStrBuilder = new StringBuilder(Tools.toString(retCode,"0"));
		logStrBuilder.append("|").append((time != null) ? ((System.currentTimeMillis() - time) + "") : "0");
		logStrBuilder.append("|").append(Tools.toString(IpUtil.getIpAddr(request), ""));
		logStrBuilder.append("|").append(request.getMethod());
    	logStrBuilder.append("|").append(request.getRequestURI());
		logStrBuilder.append("|").append(getRequestLogStr(request));
//		String uri=Tools.toString(request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE));
//		if(uri!=null){
////			RequestMetrics.getI().doCount("HttpRestApi",uri, System.currentTimeMillis()-time, e==null?true:false);
//		}
    	return logStrBuilder.toString();
    }

    protected String getLogStr(HttpServletRequest request, Long time, Object result, Exception e) {
    	return getLogStr(request, 1, time, result, e);
    }
    
//    protected boolean isInnerAddr(HttpServletRequest request) {
//
//        String ip = RequestUtils.getRemoteAddr(request);
//        if (StringUtils.isBlank(ip)) {
//            return false;
//        }
//
//		if (ip.startsWith("10.") || ip.equals("127.0.0.1") || (ip.equals("124.193.193.25") || ip.equals("103.254.65.210") && Tools.toBoolean(request.getParameter("__debug"), false))) {
//			return true;
//		}
//
//        return false;
//    }
	
}
