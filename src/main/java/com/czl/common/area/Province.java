package com.czl.common.area;


import java.util.HashMap;
import java.util.Map;

/**
 * @author 石雷
 * shilei1@jiayuan.com
 * 2020/10/13/013 11:19
 */
public enum  Province {

    _11(11,0,0,"北京市"),
    _12(12,0,0,"天津市"),
    _13(13,0,0,"河北省"),
    _14(14,0,0,"山西省"),
    _15(15,0,0,"内蒙古自治区"),
    _21(21,0,0,"辽宁省"),
    _22(22,0,0,"吉林省"),
    _23(23,0,0,"黑龙江省"),
    _31(31,0,0,"上海市"),
    _32(32,0,0,"江苏省"),
    _33(33,0,0,"浙江省"),
    _34(34,0,0,"安徽省"),
    _35(35,0,0,"福建省"),
    _36(36,0,0,"江西省"),
    _37(37,0,0,"山东省"),
    _41(41,0,0,"河南省"),
    _42(42,0,0,"湖北省"),
    _43(43,0,0,"湖南省"),
    _44(44,0,0,"广东省"),
    _45(45,0,0,"广西壮族自治区"),
    _46(46,0,0,"海南省"),
    _50(50,0,0,"重庆市"),
    _51(51,0,0,"四川省"),
    _52(52,0,0,"贵州省"),
    _53(53,0,0,"云南省"),
    _54(54,0,0,"西藏自治区"),
    _61(61,0,0,"陕西省"),
    _62(62,0,0,"甘肃省"),
    _63(63,0,0,"青海省"),
    _64(64,0,0,"宁夏回族自治区"),
    _65(65,0,0,"新疆维吾尔自治区");




    private Integer id;
    private Integer pid;
    private Integer deep;
    private String name;

    public static Map<Integer,Province> FIELDS= new HashMap<>();
    static{
        for (Province value : Province.values()) {
            FIELDS.put(value.getId(),value);
        }
    }


    Province(Integer id, Integer pid, Integer deep, String name) {
        this.id = id;
        this.pid = pid;
        this.deep = deep;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public Integer getPid() {
        return pid;
    }

    public Integer getDeep() {
        return deep;
    }

    public String getName() {
        return name;
    }
}
