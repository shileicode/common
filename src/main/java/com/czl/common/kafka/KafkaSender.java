package com.czl.common.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * 生产者
 *
 * @author haodj
 */
@Component
public class KafkaSender {
    @Autowired
    KafkaTemplate kafkaTemplate;

    /**
     *
     */
    public void sendMsg(String topic, String msg) {
        kafkaTemplate.send(topic, msg);
    }

    /**
     *
     */
    public void sendMsg(String topic, String key, String msg) {
        kafkaTemplate.send(topic, key, msg);
    }

    /**
     *
     */
    public void sendMsg(String topic, Integer partition, String key, String msg) {
        kafkaTemplate.send(topic, partition, key, msg);
    }

    /**
     *
     */
    public void sendMsg(String topic, Integer partition, Long timestamp, String key, String msg) {
        kafkaTemplate.send(topic, partition, timestamp, key, msg);
    }

//    @ResponseBody
//    @RequestMapping(value = "sendMsg")
//    public ListenableFuture sendMsg(String msg){
//        kafkaTemplate.send("topic.quick.demo", msg).get();
//    }
}